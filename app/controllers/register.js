/* Register page where someone can register itself
*/
var Cloud = require('ti.cloud');
var _DEBUG = Alloy.Globals._DEBUG;

$.btnRegister.addEventListener('click',function(e){
	if(_DEBUG){
		console.log('/register.js/ init function registerUser: ' + _DEBUG);
	}
	//Cloud standard user create function:
	Cloud.Users.create({
    username: $.username.getValue(),
    email: $.email.getValue(),
    first_name: $.firstname.getValue(),
    last_name: $.lastname.getValue(),
    password: $.password.getValue(),
    password_confirmation: $.confirmPassword.getValue()
	}, function (e) {
    	if (e.success) {
        	var user = e.users[0];
        	console.log('Success:\n' +
            	'id: ' + user.id + '\n' +
            	'sessionId: ' + Cloud.sessionId + '\n' +
            	'first name: ' + user.first_name + '\n' +
            	'last name: ' + user.last_name);
            var args = [ $.username.getValue(), $.password.getValue()];
            Alloy.createController('index', args).getView().open();
            	
    	} else {
        	console.log('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        	alert('informações preenchidas de forma incorreta, tente outra vez');
    	}
	});
});
