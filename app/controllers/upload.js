// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var Cloud = require('ti.cloud');
var args = $.args;
var _DEBUG = Alloy.Globals._DEBUG;

if(_DEBUG){
	console.log('/upload.js/ init ' + _DEBUG);
	console.log('/upload.js/ session id : ' + Cloud.sessionId);
}
