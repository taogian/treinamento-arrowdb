/* Starting page of the app where the user is able to 
 * login, register it self.
*/
var Cloud = require('ti.cloud');

// special global variable which will serve as a _DEBUG when set to true testing logs will be printed on console/txt file.
var _DEBUG = Alloy.Globals._DEBUG;
var args = $.args;
var checkbox = $.rememberme.value;

var persisted_username = Ti.App.Properties.getObject('username');
var persisted_password = Ti.App.Properties.getObject('password');
var persisted_checkbox = Ti.App.Properties.getBool('checkbox');

//check if data is set
if(_DEBUG){
	console.log(Ti.App.Properties.getObject("username"));
	console.log(Ti.App.Properties.getObject("password"));
	console.log(Ti.App.Properties.getBool('checkbox'));
}

//checks if user and password are set to remembered thus exists and if checkbox is set true
if(persisted_username != null && persisted_password != null && persisted_checkbox != false)
{
	$.user.setValue(Ti.App.Properties.getObject('username'));
	$.password.setValue(Ti.App.Properties.getObject('password'));
}





//set as checkbox
$.rememberme.setStyle(0);


if(_DEBUG)
	{
		console.log('*********************************************************');
		console.log('*********************************************************');
		console.log('/index.js/ page start DEBUG set to: ' + _DEBUG);
		console.log('/index.js/ session id : ' + Cloud.sessionId);
	}

if(args.length === 2)
{
	$.user.setValue(args[0]);
	$.password.setValue(args[1]);
}  
  
function methodLogin(){
	Cloud.Users.login({
    login: $.user.getValue(),
    password: $.password.getValue()
	}, function (e) {
    	if (e.success) {
        	var user = e.users[0]; 
        	alert('Success:\n' +
            	'id: ' + user.id + '\n' +
            	'sessionId: ' + Cloud.sessionId + '\n' +
            	'first name: ' + user.first_name + '\n' +
            	'last name: ' + user.last_name);
            	//remember me checkbox:
            	if(Ti.App.Properties.getBool('checkbox') === true){
            		Ti.App.Properties.setObject("username", user.username);
            		Ti.App.Properties.setObject("password", user.password);
            		Ti.App.Properties.setBool('checkbox', true);
            	}
            	Alloy.createController('upload').getView().open();
    } else {
        if(_DEBUG){
        	console.log('/register.js/ init function registerUser: ' + _DEBUG);
        	console.log('Error:\n'+ ((e.error && e.message) || JSON.stringify(e)));
        }
        alert('Usuário ou senha inválidos');
    }
	});	
}


function methodRegister(){
	if(_DEBUG){
		console.log('/index.js/ init function methodRegister: ' + _DEBUG);
		
	}
	Alloy.createController('register').getView().open();
}

$.index.open();
